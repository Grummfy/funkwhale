/* eslint-disable */
export default {
  "locales": [
    {
      "code": "ar",
      "label": "العربية"
    },
    {
      "code": "en_US",
      "label": "English (United-States)"
    },
    {
      "code": "en_GB",
      "label": "English (UK)"
    },
    {
      "code": "ca",
      "label": "Català"
    },
    {
      "code": "cs",
      "label": "Čeština"
    },
    {
      "code": "de",
      "label": "Deutsch"
    },
    {
      "code": "eo",
      "label": "Esperanto"
    },
    {
      "code": "es",
      "label": "Español"
    },
    {
      "code": "eu",
      "label": "Euskara"
    },
    {
      "code": "fr_FR",
      "label": "Français"
    },
    {
      "code": "gl",
      "label": "Galego"
    },
    {
      "code": "it",
      "label": "Italiano"
    },
    {
      "code": "nb_NO",
      "label": "Bokmål"
    },
    {
      "code": "nl",
      "label": "Nederlands"
    },
    {
      "code": "oc",
      "label": "Occitan"
    },
    {
      "code": "pl",
      "label": "Polszczyzna"
    },
    {
      "code": "pt_BR",
      "label": "Português (Brasil)"
    },
    {
      "code": "pt_PT",
      "label": "Português (Portugal)"
    },
    {
      "code": "ru",
      "label": "Русский"
    }
  ]
}
