# Bleeding edge Django
django>=2.2.4,<2.3

# Configuration
django-environ>=0.4,<0.5

# Images
Pillow>=5.4,<5.5

# For user registration, either via email or social
# Well-built with regular release cycles!
django-allauth>=0.39,<0.40


# Python-PostgreSQL Database Adapter
psycopg2-binary>=2.8,<=2.9

# Time zones support
pytz==2019.1

# Redis support
django-redis>=4.10,<4.11
redis>=3.2,<3.3
kombu>=4.5,<4.6

celery>=4.3,<4.4


# Your custom requirements go here
django-cors-headers>=2.5.3,<2.6
musicbrainzngs==0.6
djangorestframework>=3.10,<3.11
djangorestframework-jwt>=1.11,<1.12
pendulum>=2,<3
persisting-theory>=0.2,<0.3
django-versatileimagefield>=1.10,<1.11
django-filter>=2.1,<2.2
django-rest-auth>=0.9,<0.10
ipython>=7,<8
mutagen>=1.42,<1.43

pymemoize==1.0.3

django-dynamic-preferences>=1.7.1,<1.8
raven>=6.10,<7
python-magic==0.4.15
# XXX: until https://github.com/django/channels/issues/1240 is fixed
channels==2.1.6
channels_redis>=2.3,<2.4
daphne>=2.2,<2.3
uvicorn
gunicorn

cryptography>=2,<3
# requests-http-signature==0.0.3
# clone until the branch is merged and released upstream
git+https://github.com/EliotBerriot/requests-http-signature.git@signature-header-support
django-cleanup==3.2.0

# for LDAP authentication
python-ldap==3.2.0
django-auth-ldap==1.7.0
pydub==0.23.1

pyld==1.0.4
aiohttp==3.5.4
autobahn>=19.3.3

django-oauth-toolkit==1.2
django-storages==1.7.1
boto3<3
unicode-slugify
