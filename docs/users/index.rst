User Documentation
=====================================

This documentation is targeted at Funkwhale users. In here you'll find guides for interacting with
Funkwhale, uploading your music, and building a musical social network.

Getting Started
---------------

.. toctree::
   :maxdepth: 2

   create
   tagging
   upload


Using Funkwhale
---------------

.. toctree::
   :maxdepth: 2

   queue
   managing
   playlists
   favorites
   radios
   follow
   apps

Troubleshooting Issues
----------------------

.. toctree::
   :maxdepth: 2

   troubleshooting
