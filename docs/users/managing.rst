Managing Content and Libraries
==============================

Managing your music libraries is an important part of using Funkwhale. In addition to :doc:`uploading new music <upload>`, you may also want to :ref:`edit your library's details <edit_library>`, :ref:`delete a library <delete_library>`, or :ref:`remove content from a library <remove_content>`.

.. _edit_library:

Editing a Library
--------------------

To change details about a library:

- Navigate to ``https://your-instance/content/libraries`` or click "Add Content" under the "Music" menu, select "Upload audio content", and click "Detail" under the library you wish to edit
- Select "Edit" from the menu that appears
- In the edit menu, you will be able to change the name, description, and visibility of your library
- Make the changes you wish to make, then select "Update library" to save the changes

.. _delete_library:

Deleting a Library
------------------

.. warning::

   Deleting a library will also delete any content within the library. Make sure that content is backed up before continuing.

To delete a library:

- Navigate to ``https://your-instance/content/libraries`` or click "Add Content" under the "Music" menu, select "Upload audio content", and click "Detail" under the library you wish to edit
- Select "Edit" from the menu that appears
- Select "Delete" from the bottom of the menu. A pop up will appear warning of the consequences of deleting the library. If you want to continue, click "Delete library"

.. _remove_content:

Removing Content From a Library
-------------------------------

.. warning::

   Removing content from your library deletes the file from the server. Make sure you have a backup of any files you want to keep.

To delete content from a library:

- Navigate to ``https://your-instance/content/libraries`` or click "Add Content" under the "Music" menu, select "Upload audio content", and click "Detail" under the library you wish to edit
- Select "Tracks" from the menu that appears
- Select all tracks you wish to remove by selecting the checkboxes next to them
- In the "Actions" drop down menu, select "Delete" and click "Go". A pop up will appear warning of the consequences of deleting the library. If you want to continue, click "Launch"
