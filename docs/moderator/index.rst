Moderator Documentation
=========================

This documentation is targeted at instance moderators. Moderators have enhanced permissions
which allow them to moderate federated accounts and domains.

Moderation Guides
-----------------

.. toctree::
   :maxdepth: 2

   domains
   users
   listing

